// Modular System Puzzle

var solved = false;

var deities = [
	'Xipe-Totec',
	'Tezcatlipoca',
	'Quetzalcoatl',
	];

var image_urls = {
	'Xipe-Totec': 'Xipe-totec.svg',
	// https://commons.wikimedia.org/wiki/User:Katepanomegas

	'Tezcatlipoca': 'Tezcatlipoca.svg',
	// https://commons.wikimedia.org/wiki/User:Katepanomegas

	'Quetzalcoatl': 'Quetzalcoatl.svg',
	// https://commons.wikimedia.org/wiki/User:Eddo

}

var curr_angles = {};

var n = 10; // Number of turns
var rotation = 360 / n;

var disks = 3; // Number of disks
var donuts = ['donut1', 'donut2', 'donut3'];

var deity = "";

function reset() {

	deity = deities[Math.floor(Math.random() * deities.length)];
	var image_url = image_urls[deity]

	$("#use_image").attr("xlink:href", "images/" + image_url);

	var random_angles = Array.from({length: disks}, () => Math.floor(Math.random() * n));
	var donut_angles = Object.assign({}, ...donuts.map((n, index) => ({[n]: random_angles[index]})));

	for (let donut of donuts) {
	  $("#" + donut).rotate(rotation * donut_angles[donut]);
	  curr_angles[donut] = rotation * donut_angles[donut];
	}
}

reset();

m = {"donut1": [1, 1, 1],
	 "donut2": [0, 1, 1],
	 "donut3": [0, 0, 1]};

function isInPlace(angle, donut, donuts) {
  return angle == 0;
}

function successAlert() {
	solved = true;
	alert_text = "My name is ".concat(deity);
	alert(alert_text);	
}

$(".donut").rotate(
	{
		bind:
		{

			// Rotate clockwise on left click
			click: function(){

				for (i = 0; i < donuts.length; i++) {
					donut = donuts[i];

					$("#" + donut).rotate({
						duration:1000,
						easing: $.easing.easeInOutElastic,
						angle: curr_angles[donut],
						animateTo: curr_angles[donut] + m[this.id][i] * rotation
					});

					curr_angles[donut] = (curr_angles[donut] + m[this.id][i] * rotation) % 360
				}

				if (Object.values(curr_angles).every(isInPlace) && !solved) {
					successAlert();
				};
			},

			// Rotate counter-clockwise on right click
			contextmenu: function(e){

				// Prevent context menu from popping
				e.preventDefault();

				for (i = 0; i < donuts.length; i++) {
					donut = donuts[i];

					$("#" + donut).rotate({
						duration:1000,
						easing: $.easing.easeInOutElastic,
						angle: curr_angles[donut],
						animateTo: curr_angles[donut] - m[this.id][i] * rotation
					});

					curr_angles[donut] = (curr_angles[donut] - m[this.id][i] * rotation) % 360
				}
				
				if (Object.values(curr_angles).every(isInPlace) && !solved) {
					successAlert();
				};
			}
		}
	}
);
